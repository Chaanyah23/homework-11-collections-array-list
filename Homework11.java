import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Homework11 {

    public static void main(String[] args) {

        List<Integer> numbers = new ArrayList<>(Arrays.asList(-2, -3, 6, 3));
        List<Integer> numbers2 = new ArrayList<>(Arrays.asList(-2, -4, 5, -3));
        List<Integer> number3 = new ArrayList<>();

        // Add values from numbers array list to numbers3 array list
        for (int i = 0; i < numbers.size(); i++) {
            number3.add(numbers.get(i));
        }

        // Add values from numbers2 array list to numbers3 array list
        for (int i = 0; i <numbers2.size(); i++) {
            number3.add(numbers2.get(i));
        }

        // Sort the Array list from smallest to largest
        Collections.sort(number3);

        // Print out string array
        System.out.println(number3);

        // Print out the first value, in this case the smallest value
        System.out.println(number3.get(0));

    }

}
